﻿/// <reference path="Scripts/angular.js" />

var MyApp = angular.module("MyApp", ['ngRoute', 'GameService']);

MyApp.config(['$routeProvider',
    function ($routeProvider) {
        
        $routeProvider.
            when('/Add', {
                templateUrl: 'Views/add.html',
                controller: 'AddController'
            }).
            when('/Edit', {
                templateUrl: 'Views/edit.html',
                controller: 'EditController'
            }).
            when('/Delete', {
                templateUrl: 'Views/delete.html',
                controller: 'DeleteController'
            }).
            when('/', {
                templateUrl: 'Views/home.html',
                controller: 'HomeController'
            }).
            otherwise({
                redirectTo: '/'
            });
    }]);

MyApp.controller("AddController", function ($scope, GameApi) {

    $scope.addGame = function () {
        var gameToAdd = {
            'Name': $scope.name,
            'Price': $scope.price,
            'ImagePath': $scope.image
        };
        GameApi.addGame(gameToAdd).then(function (success) {
            alert('game added');
            $scope.name = undefined;
            $scope.price = undefined;
            $scope.image = undefined;
        }, function (error) {
            alert('error in adding');
        });
    }
});
MyApp.controller("EditController", function ($scope, GameApi) {

    $scope.selectedItem = "Select Game";
    $scope.isDeleteItemVisible = false;
    getGames();
    function getGames() {
        GameApi.getGames().then(function (success) {
            $scope.games = success.data;
        }, function (error) {
            $scope.status = 'Unable to load game data: ' + error.message;
        });
    };

    $scope.dropboxItemSelected = function (item) {
        $scope.isDeleteItemVisible = true;
        $scope.selectedItem = item.GameId;
        $scope.name = item.Name;
        $scope.price = item.Price;
        $scope.gameId = item.GameId;
        $scope.image = item.ImagePath;
    };

    $scope.updataGame = function () {
        var gameToUpdate = {
            'GameId': $scope.gameId,
            'Name': $scope.name,
            'Price': $scope.price,
            'ImagePath': $scope.image
        };

        GameApi.editGame(gameToUpdate).then(function (success) {
            alert('game updated');
            $scope.gameId = undefined;
            $scope.name = undefined;
            $scope.price = undefined;
            $scope.image = undefined;
            $scope.selectedItem = "Select Game";
            $scope.isDeleteItemVisible = false;
            getGames();
        }, function (error) {
            alert('error in updating');
        });
    };
});
MyApp.controller("DeleteController", function ($scope, GameApi) {

    $scope.selectedItem = "Select Game";
    $scope.isDeleteItemVisible = false;
    getGames();
    function getGames() {
        GameApi.getGames().then(function (success) {
            $scope.games = success.data;
        }, function (error) {
            $scope.status = 'Unable to load game data: ' + error.message;
        });
    };

    $scope.dropboxItemSelected = function (item) {
        $scope.isDeleteItemVisible = true;
        $scope.selectedItem = item.GameId;
        $scope.name = item.Name;
        $scope.price = item.Price;
        $scope.gameId = item.GameId;
        $scope.image = item.ImagePath;
    };

    $scope.deleteGame = function () {
        var gameToDelete = {
            'GameId': $scope.gameId,
            'Name': $scope.name,
            'Price': $scope.price,
            'ImagePath': $scope.image
        };

        GameApi.deleteGame(gameToDelete).then(function (success) {
            alert('game deleted');
            $scope.gameId = undefined;
            $scope.name = undefined;
            $scope.price = undefined;
            $scope.image = undefined;
            $scope.selectedItem = "Select Game";
            $scope.isDeleteItemVisible = false;
            getGames();
        }, function (error) {
            alert('error in deleting');
        });
    };
});
MyApp.controller("HomeController", function ($scope, GameApi) {

    getGames();

    function getGames() {
        GameApi.getGames().then(function (success) {
            $scope.games = success.data;
        }, function (error) {
            $scope.status = 'Unable to load game data: ' + error.message;
        });
    }
});