﻿/// <reference path="Scripts/angular.js" />

var GameService = angular.module('GameService', []);

GameService.factory('GameApi', function ($http) {

    var urlBase = "http://localhost:11105/api";
    var GameApi = {};

    GameApi.getGames = function () {

        return $http.get(urlBase + '/Games');
    };

    GameApi.addGame = function (game) {
        return $http.post(urlBase + '/Games/', game);
    };

    GameApi.editGame = function (gameToUpdate) {

        var request = $http({
            method: 'put',
            url: urlBase + '/Games/' + gameToUpdate.GameId,
            data: gameToUpdate
        });
        return request;
    };

    GameApi.deleteGame = function (gameToDelete) {

        var request = $http({
            method: 'delete',
            url: urlBase + '/Games/' + gameToDelete.GameId
        });
        return request;
    };

    return GameApi;
});