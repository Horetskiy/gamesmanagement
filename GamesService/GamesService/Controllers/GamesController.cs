﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GamesService.Models;

namespace GamesService.Controllers
{
    public class GamesController : ApiController
    {
        private GamesContext db = new GamesContext();

        // GET api/Games
        public IQueryable<Game> GetGame()
        {
            return db.Game;
        }

        // GET api/Games/5
        [ResponseType(typeof(Game))]
        public IHttpActionResult GetGame(int id)
        {
            Game game = db.Game.Find(id);
            if (game == null)
            {
                return NotFound();
            }

            return Ok(game);
        }

        // PUT api/Games/5
        public IHttpActionResult PutGame(int id, Game game)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != game.GameId)
            {
                return BadRequest();
            }

            RemarkGameData(game);
            db.Entry(game).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GameExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST api/Games
        [ResponseType(typeof(Game))]
        public IHttpActionResult PostGame(Game game)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            RemarkGameData(game);
            db.Game.Add(game);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = game.GameId }, game);
        }

        // DELETE api/Games/5
        [ResponseType(typeof(Game))]
        public IHttpActionResult DeleteGame(int id)
        {
            Game game = db.Game.Find(id);
            if (game == null)
            {
                return NotFound();
            }

            db.Game.Remove(game);
            db.SaveChanges();

            return Ok(game);
        }

        public void RemarkGameData(Game game)
        {
            RemarkPriceInGame(game);
            RemarkImagePathInGame(game);
        }
        public void RemarkPriceInGame(Game game)
        {
            var currentPrice = game.Price;
            var priceInCents = (int)(game.Price * 100);
            game.Price = priceInCents / 100.0;
        }
        public void RemarkImagePathInGame(Game game)
        {
            if (!isImagePathContainsType(game.ImagePath))
            {
                string correctPath = game.ImagePath + ".jpg";
                game.ImagePath = correctPath;
            }
        }
        private bool isImagePathContainsType(string path)
        {
            return path.Contains(".jpg") ||
                path.Contains(".png") ||
                path.Contains(".bmp");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GameExists(int id)
        {
            return db.Game.Count(e => e.GameId == id) > 0;
        }
    }
}