﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GamesService.Models
{
    public class GamesContext : DbContext
    {
        public GamesContext() : base("name = GamesConnection") { }
        public DbSet<Game> Game { get; set; }
    }
}