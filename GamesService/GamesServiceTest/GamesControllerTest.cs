﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GamesService.Controllers;
using GamesService.Models;

namespace GamesServiceTest
{
    [TestClass]
    public class GamesControllerTest
    {
        [TestMethod]
        public void TestPrice()
        {
            testCorrectPrice(4.5);
            testUncorrectPrice(9.9987, 9.99);
        }

        private void testCorrectPrice(double correct)
        {
            testUncorrectPrice(correct, correct);
        }
        private void testUncorrectPrice(double uncorrect, double correct)
        {
            var controller = new GamesController();
            var game = new Game() { Price = uncorrect };
            controller.RemarkPriceInGame(game);
            Assert.AreEqual(correct, game.Price);
        }

        [TestMethod]
        public void TestImagePath()
        {
            testCorrectImagePath("img.png");
            testUncorrectImagePath("img", "img.jpg");
        }

        private void testCorrectImagePath(string correct)
        {
            testUncorrectImagePath(correct, correct);
        }
        private void testUncorrectImagePath(string uncorrect, string correct)
        {
            var controller = new GamesController();
            var game = new Game() { ImagePath = uncorrect };
            controller.RemarkImagePathInGame(game);
            Assert.AreEqual(correct, game.ImagePath);
        }
    }
}
